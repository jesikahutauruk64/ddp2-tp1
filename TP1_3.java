///Sistem penentuan diskon dalam toko ditentukan oleh ketentuan berikut:
///Kartu tipe Gold akan memberikan 25% diskon
///Kartu tipe Silver akan memberikan 15% diskon
///Kartu tipe Bronze akan memberikan 5% diskon
///Kartu tipe lainnya tidak memberikan diskon
///Dikarenakan proses dalam penjualan toko harus cepat, maka kasir diperbolehkan untuk mengetik dengan / tanpa huruf kapital
///Kartu yang sudah berusia lebih dari 3 tahun (inklusif) akan diberikan tambahan 5% diskon [Asumsi TP1 ini diadakan pada tahun 2020]
///Untuk mempermudah, maka harga barang sudah tertanam di program yaitu 200.000 di dalam variabel hargaBarang (hanya perlu mengurusi kasus ini saja.)

import java.util.Scanner;

///ucapan selamat datang dan input
public class TP_1_Soal_3{
    public static void name(String args[]) {
        System.out.print("Selamat Datang di Bank Dedepedua")
        Scanner inp = new Scanner(System.in);
        System.out.print("Apa tipe kartu anda? ");
        String tipe = inp.nextLine();
        String Tipe = tipe.toUpperCase();
        System.out.print("Sudah berapa lama anda menjadi member? ");
        int tahun = inp.nextInt();
        int harga = 200000;

        //operasi pemberian diskon berdasarkan jenis kartu dan 
        switch (Tipe){
            case "GOLD":
                if (2020 - tahun >= 3){
                    int harga_bersih = harga - (harga*30/100);
                    System.out.print("Dapat diskon sebesar 30 persen");
                    System.out.print("Harga barang menjadi Rp " + harga_bersih + ".0");
                }
                else{
                    int harga_bersih = harga - (harga*25/100);
                    System.out.print("Dapat diskon sebesar 25 persen");
                    System.out.print("Harga barang menjadi Rp " + harga_bersih + ".0");
                }
                break;
            case "SILVER":
                if (2020 - tahun >= 3){
                    int harga_bersih = harga - (harga*20/100);
                    System.out.print("Dapat diskon sebesar 20 persen");
                    System.out.print("Harga barang menjadi Rp " + harga_bersih + ".0");
                }
                else{
                    int harga_bersih = harga - (harga*15/100);
                    System.out.print("Dapat diskon sebesar 15 persen");
                    System.out.print("Harga barang menjadi Rp " + harga_bersih + ".0");
                }
                break;    
            case "BRONZE":
                if (2020 - tahun >= 3){
                    int harga_bersih = harga - (harga*10/100);
                    System.out.print("Dapat diskon sebesar 10 persen");
                    System.out.print("Harga barang menjadi Rp " + harga_bersih + ".0");
                }
                else{
                    int harga_bersih = harga - (harga*5/100);
                    System.out.print("Dapat diskon sebesar 5 persen");
                    System.out.print("Harga barang menjadi Rp " + harga_bersih + ".0");
                }
                break;
            default:
                if (2020 - tahun >= 3){
                    int harga_bersih = harga - (harga*5/100);
                    System.out.print("Dapat diskon sebesar 5 persen");
                    System.out.print("Harga barang menjadi Rp " + harga_bersih + ".0");
                }
                else{
                    System.out.print("Tidak mendapatkan diskon");
                    System.out.print("Harga barang Rp " + harga+ ".0");
                break;         

            }
        }